<?php
namespace bosrv;

use Thrift\Protocol\TJSONProtocol;
use Thrift\Transport\THttpClient;

class Connect
{
    private $transport;
    private $protocol;

    function __construct()
    {
        $this->transport = null;
        $this->protocol = null;

        register_shutdown_function(array($this, 'disconnect'));
    }

    function __destruct()
    {
        $this->disconnect();
    }

    function connect($url, $timeout=30)
    {
        $info = parse_url($url);
        $host = $info['host'];
        $path = array_key_exists('path', $info) ? $info['path'] : '';
        $port = array_key_exists('port', $info) ? $info['port'] : 80;
        $token = array_key_exists('pass', $info) ? $info['pass'] : '';
        $schema = $info['scheme'] == 'bos' ? 'https' : 'http';

        if ($this->transport == null || !$this->transport->isOpen())
        {
            $this->transport = new THttpClient($host, $port, $path, $schema);
            $this->protocol = new TJSONProtocol($this->transport);
            $this->transport->open();
        }

        return Array($token, new BigObjectServiceClient($this->protocol));
    }

    function settimeout($timeout)
    {
        $this->transport->setTimeoutSecs($timeout);
    }

    function disconnect()
    {
        if ($this->transport != null &&
            $this->transport->isOpen())
        {
            $this->transport->close();
        }
    }
}
