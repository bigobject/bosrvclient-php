let g:ctrlp_custom_ignore = {
  \ 'dir':  'vendor$',
  \ 'file': '\.sw*$'
  \ }
