<?php
Phar::mapPhar();

require_once 'phar://bosrv.phar/Thrift/ClassLoader/ThriftClassLoader.php';
require_once 'phar://bosrv.phar/bosrv/Connect.php';

use Thrift\ClassLoader\ThriftClassLoader;

$loader = new ThriftClassLoader();
$loader->registerNamespace('Thrift', 'phar://bosrv.phar');
$loader->registerDefinition('bosrv', 'phar://bosrv.phar');
$loader->registerDefinition('exc', 'phar://bosrv.phar');
$loader->register();

__HALT_COMPILER();
