<?php
$srcRoot = '../src';
$buildRoot = './';

$phar = new Phar(
    $buildRoot.'bosrv.phar',
    FilesystemIterator::CURRENT_AS_FILEINFO | FilesystemIterator::KEY_AS_FILENAME,
    'bosrv.phar'
);

# Archive Thrift library
$phar->buildFromDirectory('/usr/lib/php');

# Archive MDAService + wrapper
foreach (glob($srcRoot.'/*', GLOB_ONLYDIR) as $dir)
{
    $dirRoot = basename($dir);
    foreach (glob($dir.'/*') as $filepath)
    {
        $phar->addFile($filepath, $dirRoot.'/'.basename($filepath));
    }
}

# Setup initializer
$phar->setStub(
    file_get_contents("setup.php")
);
