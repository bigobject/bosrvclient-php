# What is this

This is a helper script for building static phar object.

# Who needs this

If you want to contribute, or you distribute customization of your bosrvclient.

# What do you need

For your build to work, you need a local installation of Apache Thrift PHP
client library.  Apache Thrift PHP must be intalled at /usr/lib/php to work.

# How to use this

```php
require_once 'bosrv.phar'
```

Autoload will take care of the rest.
