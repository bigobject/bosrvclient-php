<?php

$newline = (PHP_SAPI == 'cli') ? PHP_EOL : '<br />';

#
# Pull in require our Big Object Serivce php client
#
if (file_exists('bosrv.phar'))
{
    require_once 'bosrv.phar';
}
else if (file_exists('vendor/autoload.php'))
{
    require_once 'vendor/autoload.php';
}
else
{
    die('Missing autoload file or phar');
}

$users = Array(
    Array(1, 'M', 'Taiwan'),
    Array(2, 'M', 'US'),
    Array(3, 'F', 'Sleepy Hollow'),
    Array(4, 'F', 'Taiwan'),
    Array(5, 'M', 'Japan'),
    Array(6, 'F', 'United Kindom'),
    Array(7, 'F', 'Scott'),
    Array(8, 'F', 'Happy Tree Valley')
);

$shoes = Array(
    Array(1, 'Toms'),
    Array(2, 'Sperry'),
    Array(3, 'Kate'),
    Array(4, 'Sofi'),
    Array(5, 'McClain'),
    Array(6, 'Cherry'),
    Array(7, 'Miu Miu'),
    Array(8, 'Salvatore Ferrogamo')
);

function setup_environ($addr_info)
{
    global $users, $shoes, $newline;
    $conn = new bosrv\Connect();
    list($token, $cli) = $conn->connect($addr_info);
    $sql_stmts = Array(
        'CREATE TABLE users (id INT, gender STRING, country STRING, KEY (id))',
        'CREATE TABLE shoes (id INT, brand STRING, KEY(id))',
        "CREATE TABLE steps (users.id INT, shoes.id INT, FACT step INT)",
    );
    foreach ($sql_stmts as $create_stmt)
    {
        $cli->execute($token, $create_stmt, '', '');
    }
    $insert_stmt = 'INSERT INTO users.bt VALUES '.implode(' ', array_map(
        function ($val) { return '('.implode(',', $val).')'; },
        $users
    ));
    $cli->execute($token, $insert_stmt, '', '');

    $insert_stmt = 'INSERT INTO shoes.bt VALUES '.implode(' ', array_map(
        function ($val) { return '('.implode(',', $val).')'; },
        $shoes
    ));
    $cli->execute($token, $insert_stmt, '', '');
}

function populate_steps($addr_info, $report=True, $count=1)
{
    global $users, $shoes, $newline;
    $conn = new bosrv\Connect();
    list($token, $cli) = $conn->connect($addr_info);

    $start = microtime(true);

    for ($cnt = 0; $cnt < $count; $cnt++)
    {
        $step_grp_val = 'INSERT INTO steps.bt VALUES '.implode(' ', array_map(
            function ($val) {
                return '('.implode(',', $val).')';
            },
            array_map(
                function ($cnt) {
                    global $users, $shoes;
                    return Array(
                        $users[array_rand($users)][0], // uid
                        $shoes[array_rand($shoes)][0], // sid
                        rand(1, 10) // step
                    );
                },
                range(1, 10000)
            )
        ));

        $cli->execute($token, $step_grp_val, '', '');
    }

    $end = microtime(true);

    if ($report)
    {
        print "--------------------------".$newline;
        print "Insert time for 10000 values: ".($end - $start).$newline;
        print "--------------------------".$newline.$newline;
    }
}
