<?php

require_once 'tables.php';

$addr_info = getenv('BIGOBJECT_URL');

setup_environ($addr_info);
populate_steps($addr_info);

#
# Create a connection object
#
$conn = new bosrv\Connect();

#
# perform connect and get back Multi-Dimensional Analysis
# service client
#
list($token, $cli) = $conn->connect($addr_info);

#
# START OF ANALYTIC COMPUTATION DEMO
#
# We have now a few records in our fact table regarding the steps taken at
# random interval from our user base and their preferred sneakers.  We want to
# know the steps taken from each person
#

$select_stmt = 'SELECT SUM(step) FROM steps GROUP BY users.gender, shoes.brand';

$start = microtime(true);
$table = $cli->execute($token, $select_stmt, '', '');
$end = microtime(true);

print '--------------------------'.$newline;
print $select_stmt.$newline;
print 'Operation took time: '.($end - $start).$newline;
print '--------------------------'.$newline.$newline;

$eol = 100;
$result_table = Array();
$rngspec = new bosrv\RangeSpec(Array('page' => 100));

$start = microtime(true);
while ($eol != -1)
{
    $chunk = json_decode($cli->cursor_fetch(
        $token,
        $table,
        $rngspec
    ));
    list($eol, $rows) = $chunk;
    $result_table = array_merge($result_table, $rows);
}
$cli->cursor_close($token, $table);
$end = microtime(true);

print '--------------------------'.$newline;
print 'Time spent to retrieve from cursor: '.($end - $start).$newline;
foreach ($result_table as $row)
{
    print '['.implode(' ', $row).']'.$newline;
}
print '--------------------------'.$newline.$newline;
