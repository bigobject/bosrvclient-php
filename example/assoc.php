<?php

require_once 'tables.php';

$addr_info = getenv('BIGOBJECT_URL');

setup_environ($addr_info);
populate_steps($addr_info);

#
# Create a connection object
#
$conn = new bosrv\Connect();

#
# perform connect and get back Multi-Dimensional Analysis
# service client
#
list($token, $cli) = $conn->connect($addr_info);

$build_stmt_qbo = "build association q3(shoes.brand) by users.id from steps";
$start = microtime(true);
$cli->execute($token, $build_stmt_qbo, '', '');
$end = microtime(true);

print '--------------------------'.$newline;
print $build_stmt_qbo.$newline;
print 'Operation took time: '.($end - $start).$newline;
print '--------------------------'.$newline.$newline;

$query_stmt = "get 10 freq('Sperry') from q3";
$start = microtime(true);
$table = $cli->execute($token, $query_stmt, '', '');
$end = microtime(true);

print '--------------------------'.$newline;
print $query_stmt.$newline;
print 'Operation took time: '.($end - $start).$newline;
print '--------------------------'.$newline.$newline;

$eol = 100;
$result_table = Array();
$rngspec = new bosrv\RangeSpec(Array('page' => 100));

$start = microtime(true);
while ($eol != -1)
{
    $chunk = json_decode($cli->cursor_fetch(
        $token,
        $table,
        $rngspec
    ));
    list($eol, $rows) = $chunk;
    $result_table = array_merge($result_table, $rows);
}
$cli->cursor_close($token, $table);
$end = microtime(true);

print '--------------------------'.$newline;
print 'Time spent to retrieve from cursor: '.($end - $start).$newline;
foreach ($result_table as $row)
{
    print '['.implode(' ', $row).']'.$newline;
}
print '--------------------------'.$newline.$newline;
