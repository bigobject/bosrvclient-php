# BigObject Service client

This is the RPC client for BigObject Service powered by Apache Thrift.

- [What is BigObject](http://docs.macrodatalab.com/)
- [Get BigObject on HEROKU](https://addons.heroku.com/bigobject)

# Getting this package

The recommended way to obtain latest build from this repo is through
[composer](https://getcomposer.org/)

```
$ cat >composer.json <<EOF
.. {
..    "require": {
..         "macrodata/bosrvclient": "dev-master"
..     }
.. }
.. EOF

$ composer install
```

# Basic usage instructions

bosrvclient utilizes HTTP/HTTPS transport and Apache Thrift JSON protocol for
delivering and recving data.

To create a connection helper, start by including **bosrvclient**

```php
# Autoload from composer installation
require_once 'vendor/autoload.php';

$conn = new bosrv\Connect();
list($token, $client) = $conn->connect($addr_info);
```

A typical query structure can be layout into the following:

- Specify and send your query via execute method
- For methods returning a handle (sha256 string), use cursor helpers to get
  data back
- Data retrieved are encoded as JSON string.

```php
$res = $client->execute($token, <stmt>, "", "");

$rngspec = new bosrv\RangeSpec(Array('page' => 100));
$eol = null;

while ($eol != -1)
{
    $data = json_decode($client->cursor_fetch(
        $token,
        $res,
        $rngspec
    ));
    list($eol, $rows) = $data;
}
```

## Race conditions with cursor access

While all operations can be interleaved, *cursor* keep track of access progress
through internal data update.  To avoid race condition, specify *start* to in
**RangeSpec** to take hold of indexing at each *cursor\_fetch*.

# Handling resource references

We strongly urge you to close the returned resource.  While garbage collection
is done routinely on the BigObject service, it intended for unexpected
termination from client application, hence the garbage collection cycle is
kept at a minimal pace.

While it is valid to cache the returned resource handle, make sure your
application handles exceptions accordingly.

# Exceptions produced by API during runtime

## AuthError

produced when providing an invalid authentication token

## ServiceError

general mishaps or signs of really bad server state
